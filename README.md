# Kustomize - cert-manager
[cert-manager](https://cert-manager.io/docs/) adds certificates and certificate issuers as resource types in Kubernetes clusters, and simplifies the process of obtaining, renewing and using those certificates.

## Credentials
Credentials are managed through GitLab with [External Secrets](https://github.com/external-secrets/external-secrets).

See [Kubernetes - Credentials](https://wiki.tramonte.us/en/kubernetes#credentials) on the wiki for more information.
